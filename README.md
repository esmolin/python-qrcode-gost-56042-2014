# python-gost56042-2014

использование:

    from gostqrcode import GOSTQRCode
    # чтение
    code = GOSTQRCode.from_string("ST00012|Name=Пупкин|PersonalAcc=12345678901234567890")
    print(code.separator)
    >>> '|'
    print(code.fields['Name'])
    >>> 'Пупкин'

    # создание
    from collections import OrderedDict
    code = GOSTQRCode(fields=OrderedDict([('Name', 'Пупкин'), ('PersonalAcc', '12345678901234567890'), ....]))
    print(code.to_string())
    >>> 'ST00012|Name=Пупкин|PersonalAcc=12345678901234567890'

