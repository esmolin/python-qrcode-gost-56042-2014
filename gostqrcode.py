"""
модуль работы с QR-кодами по ГОСТ Р 56042-2014
FIXME: кодировки, если нужно
"""
from collections import OrderedDict
from copy import copy

class GOSTQRCode:
    """
    использование:
    # чтение
    code = GOSTQRCode.from_string("ST00012|Name=Пупкин|PersonalAcc=12345678901234567890")
    print(code.separator)
    >>> '|'
    print(code.fields['Name'])
    >>> 'Пупкин'

    # создание
    from collections import OrderedDict
    code = GOSTQRCode(fields=OrderedDict([('Name', 'Пупкин'), ('PersonalAcc', '12345678901234567890'), ....]))
    print(code.to_string())
    >>> 'ST00012|Name=Пупкин|PersonalAcc=12345678901234567890'
    """

    PREFIX = "ST"
    VERSION = "0001"
    CODING_CP1251 = "1"
    CODING_UTF8 = "2"
    CODING_KOI8R = "3"
    ALLOWED_CODINGS = [CODING_KOI8R, CODING_UTF8, CODING_KOI8R]
    DEFAULT_SEPARATOR = "|"
    KEY_VALUE_SEPARATOR = "="

    MANDATORY_FIELDS = [
        # name, length, format
        {'name': 'Name', 'min_len':1, 'max_len': 160, 'format': '{}'},
        {'name': 'PersonalAcc', 'min_len':20, 'max_len': 20, 'format': '{:0>20}'},
        {'name': 'BankName', 'min_len':1, 'max_len': 45, 'format': '{}'},
        {'name': 'BIC', 'min_len':9, 'max_len': 9, 'format': '{:0>9}'},
        {'name': 'CorrespAcc', 'min_len':1, 'max_len': 20, 'format': '{}'},
    ]


    separator = DEFAULT_SEPARATOR
    coding = CODING_UTF8
    fields = {}

    def __init__(self, coding=CODING_UTF8, separator=DEFAULT_SEPARATOR, fields=None):
        self.coding = coding
        self.separator = separator
        self.fields = fields

    @staticmethod
    def from_string(code):
        expected_head = GOSTQRCode.PREFIX + GOSTQRCode.VERSION
        if not code.startswith(expected_head):
            raise GOSTQRError('Unknown standard prefix or version: "{0}"'.format(
                code[0:len(expected_head)]))
        gostqr = GOSTQRCode(coding=code[len(expected_head):(len(expected_head)+1)],
            separator=code[(len(expected_head)+1):(len(expected_head)+2)], fields=OrderedDict())
        for frag in code[len(expected_head)+2:].split(gostqr.separator):
            k, v = frag.split(GOSTQRCode.KEY_VALUE_SEPARATOR, 1)
            gostqr.fields[k] = v

        return gostqr

    def to_string(self, ignore_mandatory=True):
        """
        строковое представление кода

        @param bool ignore_mandatory игнорировать проверку наличия обязательных полей
        @return str
        """
        result = [self.PREFIX, self.VERSION, self.coding, self.separator]
        fields = copy(self.fields)
        
        # mandatory fields
        for mf in GOSTQRCode.MANDATORY_FIELDS:
            try:
                value = fields.pop(mf['name'])
            except KeyError as ex:
                if not ignore_mandatory:
                    raise GOSTQRError(str(ex))
                else:
                    continue
            result.append(mf['name'])
            result.append(self.KEY_VALUE_SEPARATOR)
            result.append(mf['format'].format(value))
            result.append(self.separator)
        
        # other fields
        for k, v in fields.items():
            result.append(k)
            result.append(self.KEY_VALUE_SEPARATOR)
            result.append(str(v))
            result.append(self.separator)
        
        # remove last separator
        result.pop()
        return "".join(result)
        
    def __str__(self):
        return self.to_string(True)

    def __repr__(self):
        return "GOSTQRCODE('{}', '{}', {})".format(self.coding, self.separator, self.fields)

class GOSTQRError(Exception):
    pass
